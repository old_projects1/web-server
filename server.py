import socket
import random
import sys
import os
from subprocess import Popen, PIPE

# seed random for random port number
#random.seed()

# constants from apache
LIMITREQUESTFIELDS = 50
LIMITREQUESTFIELDSIZE = 4094
LIMITREQUESTLINE = 8190

# Path constant default
PATH = os.path.dirname(os.path.abspath(__file__))+"\public"
if not os.path.exists(PATH):
	PATH = os.path.dirname(os.path.abspath(__file__))+"/public"
	if not os.path.exists(PATH):
		PATH = os.path.dirname(os.path.abspath(__file__))


# PATH TO PHP interpreter
phpInterpreter = os.path.dirname(os.path.abspath(__file__))+"\cgi-bin\php.exe"

# if not found, report then exit
if not os.path.exists(phpInterpreter):
	print "php.exe not found in expected path"
	sys.exit()



# Error Dictionary
error = {"400": "Bad Request", "403": "Forbidden", "404": "Not Found", "405": "Method Not Allowed", "413": "Request Entity Too Large", "414": "Request- URI Too Long", \
		 "418": "I'm a Teapot", "500": "Internal Server Error", "501": "Not Implemented", "505": "Http Version Not Sup"}


port
if sys.argv[1] is not None:
	port = int(sys.argv[1])

# Port -defaults to 9999
else:
	port = 9999


def typeLookup(extension):
	
	if extension == "css":
		return "text/css"

	if extension == "html":
		return "text/html"

	if extension == "gif":
		return "image/gif"

	if extension == "ico":
		return "image/x-icon"

	if extension == "jpg":
		return "image/jpeg"

	if extension == "js":
		return "text/javascript"

	if extension == "png":
		return "image/png"

	if extension == "php":
		return "text/html"
	
	return None


def validate(request, connect):
	
	try:
		# potential bug
		if request is None or request == "":
			pass

		elif len(request)-1 > LIMITREQUESTLINE + LIMITREQUESTFIELDS * LIMITREQUESTFIELDSIZE:
			pass
		else:
			# extract request line
			request = request.split("\r\n")[0]
			request_line = request.split(" ")

			if len(request_line) != 3:
				connect.send(errorBuilder(400))
				return False

			# extract request type
			request_type = request_line[0]

			if request_type != "GET":
				connect.send(errorBuilder(405))
				return False

			http_version = request_line[2]

			if http_version != "HTTP/1.1":
				connect.send(errorBuilder(505))
				return False

			# request line valadation
			request_target = request_line[1]

			if request_target[0] != '/':
				connect.send(errorBuilder(400))
				return False

			if '"' in request_target:
				connect.send(errorBuilder(400))
				return False
			
			request_target = request_target.split("?")
			absolute_path = request_target[0]
			query = ""

			# If request target contains absolute path and query
			if len(request_target) == 2:
				if request_target[1] != "" or request_target[1] is not None:
					query = request_target[1]

			# If request is "/" or shortcode for index.html/index.php
			if absolute_path == "/":
				index = slashHandler()
				if index == 404:
					connect.send(errorBuilder(404))
					return False
				else:
					absolute_path = "".join([absolute_path, index])

			if '.' not in absolute_path:
				connect.send(errorBuilder(501))
				return False

			extension = absolute_path.split(".")[1]

			content_type = typeLookup(extension)

			if content_type == "" or content_type is None:
				connect.send(errorBuilder(501))
				return False

			return {"type":content_type, "abpath": absolute_path, "query": query, "extension": extension}

	except Exception, e:
		print str(e) + "\nIn validate"


def start():
	web_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	web_socket.bind(('localhost',port))
	web_socket.listen(1)
	print "Server listening on port: " + str(port) + "\n\n"
	try:
		while True:
			connect, address = web_socket.accept()
			new_request = connect.recv(LIMITREQUESTLINE)
			print new_request
			requestHandler(new_request, connect)
	except Exception, e:
		print e
	except socket.timeout, e:
		pass
	except KeyboardInterrupt, k:
		print "Program terminated by keyboard interrupt."
		web_socket.close()
		sys.exit()




def requestHandler(request, connect):
	request_content = validate(request, connect)
	if request_content is not False and request_content is not None:
		content = responseBuilder(request_content)
		respond(content, connect)


def responseBuilder(request_content):
	global PATH
	content_path = ''
	content = ''

	if not os.path.exists(PATH):
		return errorBuilder(500)

	if type(request_content) is not dict:
		return errorBuilder(400)

	content_path = ''.join([PATH, request_content["abpath"]])
	if not os.path.exists(content_path):
		return errorBuilder(404)

	print request_content["query"]
	if ".php" in content_path:
		try:
			content = phpHandler(request_content["query"], content_path)
		except Exception, e:
			return errorBuilder(404)


	else:
		try:
			with open(content_path,"rb") as f :
				content = f.read()

			if content is None or content == "":
				return errorBuilder(404)
		
		except Exception, e:
			print e
		

	response = """HTTP/1.1 200 OK\r\nConnection: close\r\nContent-Length: %i\r\nContent-Type: %s\r\n\r\n%s""" % \
	(len(content), request_content["type"], content)
	return response
		
		

def respond(response, connect):
	if type(response) is str and response != "" or response is not None:
		try:
			connect.send(response)
		except Exception, e:
			print e


def errorBuilder(code):
	template = "HTTP/1.1 %i %s\r\nContent-Type: text/html\r\nStatus: %i %s\r\n\r\n<html><head><title>%i %s</title></head><body><h1>%i %s</h1></body></html>"

	for char in ["%s", "%i"]:
		while char in template:
			if char == "%s":
				template = template.replace(char,error[str(code)])
			else:
				template = template.replace(char,str(code))

	return template


def slashHandler():
	global PATH

	if os.path.exists("\\".join([PATH,"index.html"])):
		return "index.html"
	elif os.path.exists("\\".join([PATH,"index.php"])):
		return "index.php"
	else:
		return 404


def phpHandler(query, file_path):
	global phpInterpreter

	# If there is a query, parse the query, write the query data and the data in the file_path
	# to a new, temporary file and execute that file.
	# else execute the data in the old file_path

	if query != "":
		head = "<?php $_GET = [" # head of array syntax
		tail = "]?>" # tail of array syntax

		GET_varList = []
		requestFileContents = ""

		# get contents of the requested PHP file
		with open(file_path, "r") as requestFile:
			requestFileContents = requestFile.read()


		# Extract each parameter from the query and append them to GET_varList
		query = query.split("&")

		for variable in query:
			variable = variable.split("=")
			GET_varList.append("\"%s\" => \"%s\"" % (variable[0], variable[1]))

		
		# generate body of assoc array
		body = ",".join(GET_varList)

		# Generate the _GET associative array
		GET_assocArray = head+body+tail

		print GET_assocArray
		# Create temporary file for phpInterpreter to execute
		with open("__exservgen_temp.php", "w") as exefile:
			exefile.write(GET_assocArray)
			exefile.write("\n")
			exefile.write(requestFileContents)

		# set file path as the new file created for the interpreter to execute
		file_path = os.path.dirname(os.path.realpath(__file__)) + "\\__exservgen_temp.php"



	# generate output from php interpreter
	phpProcess = Popen([phpInterpreter,"-f", "%s" % (file_path,)], stdout=PIPE, stderr=PIPE)
	stdout, stderr = phpProcess.communicate()
	print stderr, stdout

	# CLEANUP
	if "__exservgen_temp" in file_path:
		os.remove("__exservgen_temp.php")
	return stdout
	# return subprocess.check_output(command)



start()


# command = "QUERY_STRING=\"%s\" REDIRECT_STATUS=200 SCRIPT_FILENAME=\"%s\" php-cgi" % (query, path)